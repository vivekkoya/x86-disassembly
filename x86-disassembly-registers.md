# x86 Registers
* **rdi** is a register that stores the destination index of other instructions.<br>
* **rsi** is a register that stores the source index of other instructions.<br>
* **eax** is the accumulator register, similar to a temporary register. It is used by logical operands to store arithmetic results in the registers.<br>
* **edi** is similar to **rdi**, but it only has 32 bits compared to rdi's 64 bits.<br>
* **esi** is similar to **rsi**, but it only has 32 bits compared to rsi's 64 bits.<br>
* **rbx** acts as the frame pointer. It is used to keep track of other variables as the stack pointer moves throught the cache.<br>
* **ebx** is similar to **rbx**, but it only has 32 bits compared to rbx's 64 bits.<br>
* **All** of the registers that is prefixed by E are a part of the same register with the R prefix.
